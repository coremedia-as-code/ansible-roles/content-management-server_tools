# CoreMedia - command line tool for content-management-server

## usage

```
$ cd /opt/coremedia/content-management-server-tools
$ bin/cm version
1904.2 - 14.06.2019 - 03:27:12
$ bin/cm systeminfo -u $CLI_USER -p $CLI_PASSWORD

client information:
  release: 1904.2
  host: backend (192.168.124.20)
  os: Linux 3.10.0-957.27.2.el7.x86_64 (amd64)
  jvm: OpenJDK 64-Bit Server VM 1.8.0_222 (mixed mode)
  java home: /usr/lib/jvm/java-1.8.0-amazon-corretto/jre
  processors: 1

content server information:
  release: 1904.2
  host: backend.cm.local (192.168.124.20)
  HTTP port: 40180
  ORB port: 40183
  install home: /opt/coremedia/content-management-server/current/webapps/content-management-server/WEB-INF
  os: Linux 3.10.0-957.27.2.el7.x86_64 (amd64)
  jvm: OpenJDK 64-Bit Server VM 1.8.0_222 (mixed mode)
  jvm memory: 503 MByte
  java home: /usr/lib/jvm/java-1.8.0-amazon-corretto/jre
  processors: 1
  content management server: true
  multi site content management server: false
  master live server: false
  replication live server: false
  runlevel: online
  initial setup complete: true
  base home folder: /Home (coremedia:///cap/content/5)
  system folder: /System (coremedia:///cap/content/3)
  search enabled: true

user domains: []
```
